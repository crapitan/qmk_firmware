#include "pitan.h"

/*

┌───┬───┬───┐
│   │   │   │
├───┴─┬─┴───┤
│     │     │
└─────┴─────┘

┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐
│ 1 │ 2 │ 3 │ 4 │ 5 │ 6 │ 7 │ 8 │ 9 │10 │11 │12 │   │       │ │   │   │   │
├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┴───┴───┤
│     │ Q │ w │ E │ R │ T │   │   │   │   │   │   │   │     │
├───┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬───┤ ├───┴───┴───┤
│   │   │   │   │   │   │   │   │   │   │   │   │   │     │
├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┴───┴───┤
│     │   │   │   │   │   │   │   │   │   │   │   │   │     │
└─────┴───┴───┘───┘───┘───┘───┘───┘───┘───┘───┘───┘───┘─────┘

*/


const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[0] = KEYMAP( /* Base */
  KC_ESC, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_KP_PLUS, KC_KP_MINUS, KC_EQL, KC_NUHS, KC_BSPC, KC_A, KC_A, KC_A, \
  KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_LBRC, KC_RBRC, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LANG1, KC_A, KC_MENU, KC_UNDO, KC_AGAIN,  \
  KC_A, KC_A, KC_S, KC_D, KC_F, KC_G, KC_8, KC_9, KC_SCOLON, KC_H,  KC_J, KC_K, KC_L, KC_SCOLON, KC_DELETE, KC_COPY, KC_CUT, KC_PASTE,  \
  KC_LSHIFT, KC_COMM, KC_Z, KC_X, KC_C, KC_V, KC_LBRC, KC_RBRC, KC_B, KC_N, KC_M,  KC_DOT, KC_COMM, KC_RSHIFT, KC_HOME, KC_UP, KC_END,  \
  KC_LCTL, KC_APPLICATION, KC_RALT, KC_2, KC_SPC, KC_ENT, KC_SCLN, KC_A, KC_MENU, KC_RCTL,  KC_LEFT, KC_DOWN, KC_RIGHT,  \
  ),
};

const uint16_t PROGMEM fn_actions[] = {

};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
  // MACRODOWN only works in this function
      switch(id) {
        case 0:
          if (record->event.pressed) {
            register_code(KC_RSFT);
          } else {
            unregister_code(KC_RSFT);
          }
        break;
      }
    return MACRO_NONE;
};


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  return true;
}

void led_set_user(uint8_t usb_led) {

}
