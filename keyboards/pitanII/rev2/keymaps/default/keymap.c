#include "pitan.h"

// Tap Dance Declarations
enum {
  ENT_5 = 0,
  ZERO_7
};

// Macro Declarations
enum {
  DBL_0 = 0
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = { \
[0] = KEYMAP(KC_1, KC_2, KC_3, KC_1, KC_4, KC_1, KC_6, KC_1, KC_1, KC_8, KC_1, KC_1),
[1] = KEYMAP(KC_1, KC_2, KC_3, KC_1, KC_4, KC_1, KC_6, KC_1, KC_1, KC_8, KC_1, KC_1)
};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt) {
  if (record->event.pressed) {
      switch(id) {
          case DBL_0:
              SEND_STRING("00");
              return false;
      }
  }
  return MACRO_NONE;
};
