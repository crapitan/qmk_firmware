#include "pitan.h"

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
[0] = KEYMAP(M(0), M(1))};

const uint16_t PROGMEM fn_actions[] = {

};

const macro_t *action_get_macro(keyrecord_t *record, uint8_t id, uint8_t opt)
{
     if (record->event.pressed) {  // MACRODOWN only works in this function
      switch(id) {
         case 0:
                return MACRO(T(B), T(5), T(2), T(ESC), T(ESC), END);
         case 1:
                return MACRO(T(B), T(6), T(4), T(5), T(3), T(ESC), T(ESC), END);
      }
    }
    return MACRO_NONE;
};


void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  return true;
}

void led_set_user(uint8_t usb_led) {
}