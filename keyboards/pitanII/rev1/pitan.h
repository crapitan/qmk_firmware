#ifndef PITAN_H
#define PITAN_H

#include "quantum.h"

// This a shortcut to help you visually see your layout.
// The following is an example using the Planck MIT layout
// The first section contains all of the arguements
// The second converts the arguments into a two-dimensional array
#define KEYMAP(k01, k02) \
{ \
   { k01, k02 }, \
}
#endif
